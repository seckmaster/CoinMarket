//
//  RefreshableTableViewController.swift
//  CoinMarket
//
//  Created by Toni Kocjan on 04/11/2017.
//  Copyright © 2017 Toni Kocjan. All rights reserved.
//

import UIKit

class RefreshableTableViewController: BaseViewController {
    
    // MARK: - Properties
    
    var userDefaults = UserDefaults.standard
    
    @IBOutlet internal var tableView: UITableView!
    internal var refreshControl: UIRefreshControl!
    internal var currentCurrency = CurrencyType.USD
    internal var currentLimit = Constants.UI.defaultCurrenciesLimit
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupRefreshControl()
        updateFilters()
    }

    // MARK: - Instance Methods
    
    @IBAction func pullToRefresh() {
        reloadData(pullToRefresh: true)
    }
    
    internal func reloadData(pullToRefresh: Bool=false) {
        guard pullToRefresh || shouldReloadData() else {
            stopRefreshing()
            return
        }
        
        print("Reloading data")
        updateFilters()
        startRefreshing()
        loadData()
    }
    
    internal func loadData() {
        assert(false, "Override this method")
    }
    
    internal func shouldReloadData() -> Bool {
        assert(false, "Override this method")
    }
    
    internal func convertToCurrencyFromUserDefaultsOrDefault() -> CurrencyType {
        let defaultIndex = 0
        let index = userDefaults.value(forKey: Constants.UI.selectedCurrencyIndexKey) as? Int
        return CurrencyType.types[index ?? defaultIndex]
    }
    
    internal func currenciesLimitFromUserDefaultsOrDefault() -> Int {
        let defaultLimit = Constants.UI.defaultCurrenciesLimit
        let limit = userDefaults.value(forKey: Constants.UI.currenciesLimitKey) as? Int
        return limit ?? defaultLimit
    }
    
    internal func updateFilters() {
        currentCurrency = convertToCurrencyFromUserDefaultsOrDefault()
        currentLimit = currenciesLimitFromUserDefaultsOrDefault()
    }
}

// MARK: - Refreshable

extension RefreshableTableViewController: Refreshable {

    internal func setupRefreshControl() {
        refreshControl = UIRefreshControl()
        refreshControl.endRefreshing()
        refreshControl.addTarget(self, action: #selector(pullToRefresh), for: .valueChanged)
        tableView.refreshControl = refreshControl
    }
}
