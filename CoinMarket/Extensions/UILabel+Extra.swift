//
//  UILabel+Extra.swift
//  CoinMarket
//
//  Created by Toni Kocjan on 04/11/2017.
//  Copyright © 2017 Toni Kocjan. All rights reserved.
//

import UIKit

extension UILabel {
    
    func style(size: CGFloat, type: FontType, color: UIColor) {
        self.font = .create(type: type, size: size)
        self.textColor = color
    }
}
