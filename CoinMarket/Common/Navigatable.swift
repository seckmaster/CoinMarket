//
//  Navigatable.swift
//  CoinMarket
//
//  Created by Toni Kocjan on 06/11/2017.
//  Copyright © 2017 Toni Kocjan. All rights reserved.
//

import UIKit

enum NavigationBarButtonType {
    case hidden, back, settings
    
    var iconName: String? {
        get {
            if self == .back { return "backButton" }
            if self == .settings { return "iconSetting" }
            return nil
        }
    }
}

protocol Navigatable {
    
    var leftBarButtonType: NavigationBarButtonType { get set }
    var rightBarButtonType: NavigationBarButtonType { get set }
    
    func hideLeftBarButton()
    func hideRightBarButton()
    func setLeftBarButton()
    func setRightBarButton()
}

extension Navigatable where Self: UIViewController {
    
    func hideLeftBarButton() {
        navigationItem.leftBarButtonItem = nil
    }
    
    func hideRightBarButton() {
        navigationItem.rightBarButtonItem = nil
    }
    
}
