//
//  UIColor+Extra.swift
//  CoinMarket
//
//  Created by Toni Kocjan on 04/11/2017.
//  Copyright © 2017 Toni Kocjan. All rights reserved.
//

import UIKit

extension UIColor {
    
    convenience init(r: CGFloat, g: CGFloat, b: CGFloat, a: CGFloat = 1) {
        self.init(red: r/255.0, green: g/255.0, blue: b/255.0, alpha: a)
    }
    
    // MARK: - Theme colors
    
    static let greyishBrown = UIColor(r: 71,g: 71,b: 71)
    static let warmGrey = UIColor(r: 125, g: 125, b: 125)
    static let bitcoinOrange = UIColor(r: 255, g: 153, b: 0)
}
