//
//  CurrencyKeyValueCell.swift
//  CoinMarket
//
//  Created by Toni Kocjan on 05/11/2017.
//  Copyright © 2017 Toni Kocjan. All rights reserved.
//

import UIKit
import SnapKit

class CurrencyKeyValueCell: UITableViewCell {
    
    // MARK: - Properties
    
    private lazy var stackView = UIStackView()
    private let inset: CGFloat = 20.0
    var data: [KeyValue]? {
        didSet { didSetData() }
    }

    // MARK: - Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupStackView()
    }
    
    // MARK: - Instance Methods
    
    private func setupStackView() {
        stackView.axis = .vertical
        stackView.spacing = 5
        stackView.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(stackView)
        addConstraints()
    }
    
    private func addConstraints() {
        stackView.snp.makeConstraints { (make) in
            make.top.equalToSuperview().inset(inset)
            make.bottom.equalToSuperview().inset(inset)
            make.left.equalToSuperview().inset(inset)
            make.right.equalToSuperview().inset(inset)
        }
    }
    
    private func didSetData() {
        if isStackViewEmpty() {
            addSubviewsToStackView()
        }
        else {
            updateStackView()
        }
    }
        
    private func isStackViewEmpty() -> Bool {
        return stackView.arrangedSubviews.isEmpty
    }
    
    private func addSubviewsToStackView() {
        data?.forEach(addKeyValueView)
    }
    
    private func addKeyValueView(_ keyValue: KeyValue) {
        let view = keyValueView(for: keyValue)
        style(keyValueView: view)
        addArrangedSubview(view)
    }
    
    private func keyValueView(for keyValue: KeyValue) -> KeyValueView {
        let keyValueView = KeyValueView()
        update(keyValueView: keyValueView, with: keyValue)
        return keyValueView
    }
    
    private func style(keyValueView view: KeyValueView) {
        style(titleLabel: view.keyLabel)
        style(valueLabel: view.valueLabel)
    }
    
    private func style(titleLabel label: UILabel) {
        label.style(size: 13, type: .bold, color: .greyishBrown)
    }
    
    private func style(valueLabel label: UILabel) {
        label.style(size: 13, type: .regular, color: .warmGrey)
    }
    
    private func addArrangedSubview(_ view: UIView) {
        stackView.addArrangedSubview(view)
    }
    
    private func updateStackView() {
        guard let data = data else { return }
        for (index, view) in stackView.arrangedSubviews.enumerated() {
            guard index < data.count, let keyValueView = view as? KeyValueView else { break } // TODO: - In this case, next keyValueViews should probably be removed
            update(keyValueView: keyValueView, with: data[index])
        }
    }
    
    private func update(keyValueView: KeyValueView, with keyValue: KeyValue) {
        keyValueView.keyLabel.text = keyValue.key
        keyValueView.valueLabel.text = keyValue.value
    }
}
