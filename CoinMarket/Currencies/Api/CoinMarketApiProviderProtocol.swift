//
//  ApiFacadeProtocol.swift
//  CoinMarket
//
//  Created by Toni Kocjan on 05/11/2017.
//  Copyright © 2017 Toni Kocjan. All rights reserved.
//

import Foundation

protocol CoinMarketApiProviderProtocol {
    
    func getCurrencies(completion: @escaping CompletionHandler)
    func getCurrencies(limit: Int, completion: @escaping CompletionHandler)
    func getCurrencies(limit: Int, currencyId: String, completion: @escaping CompletionHandler)
    func getCurrencies(limit: Int, currencyId: String, convertToCurrency: String, completion: @escaping CompletionHandler)
    func getCurrencies(limit: Int, convertToCurrency: String, completion: @escaping CompletionHandler)
    
    func getCurrency(currencyId: String, completion: @escaping (Currency?, Int)->())
    func getCurrency(currencyId: String, convertToCurrency: String, completion: @escaping (Currency?, Int)->())
}
