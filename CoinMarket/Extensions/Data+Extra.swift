//
//  Data+Extra.swift
//  CoinMarket
//
//  Created by Toni Kocjan on 05/11/2017.
//  Copyright © 2017 Toni Kocjan. All rights reserved.
//

import Foundation
import ObjectMapper

extension Data {
    
    // for testing purposes
    func toCurrencies() -> [Currency]? {
        guard let jsonObject = try? JSONSerialization.jsonObject(with: self, options: []) else { return nil }
        return Mapper<Currency>().mapArray(JSONObject: jsonObject)
    }
}
