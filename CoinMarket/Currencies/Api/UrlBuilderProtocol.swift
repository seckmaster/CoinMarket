//
//  Builder.swift
//  CoinMarket
//
//  Created by Toni Kocjan on 04/11/2017.
//  Copyright © 2017 Toni Kocjan. All rights reserved.
//

import Foundation

protocol UrlBuilderProtocol: class {
    
    func build() -> String?
}

protocol CoinMarketUrlBuilderProtocol: UrlBuilderProtocol {
    
    var baseUrl: String? { get set }
    var currencyId: String? { get set }
    var convertToCurrency: String? { get set }
    var limit: Int? { get set }
}
