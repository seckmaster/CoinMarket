//
//  CurrencyListDataBuilder.swift
//  CoinMarket
//
//  Created by Toni Kocjan on 05/11/2017.
//  Copyright © 2017 Toni Kocjan. All rights reserved.
//

import Foundation

class CurrencyListDataBuilder: CurrencyDataBuilderProtocol {
    
    func build(from currency: Currency, convertToCurrency convert: CurrencyType?) -> [KeyValue] {
        var data = [KeyValue]()
        data.append((key: "Rank:", value: currency.rank))
        data.append((key: "Symbol:", value: currency.symbol))
        let priceCurrency = convert?.rawValue ?? CurrencyType.USD.rawValue
        data.append((key: "Price [\(priceCurrency)]:", value: price(for: currency, convertToCurrency: convert)))
        data.append((key: "24h change:", value: currency.percentChange_24h))
        return data
    }
    
    private func price(for currency: Currency, convertToCurrency convert: CurrencyType?) -> String? {
        if let convert = convert {
            if convert == .CNY { return currency.priceCny }
            if convert == .EUR { return currency.priceEur }
        }
        return currency.priceUsd
    }
}
