//
//  SettingsViewController.swift
//  CoinMarket
//
//  Created by Toni Kocjan on 05/11/2017.
//  Copyright © 2017 Toni Kocjan. All rights reserved.
//

import UIKit

class SettingsViewController: BaseViewController {
    
    // MARK: - Properties
    
    var userDefaults = UserDefaults.standard

    @IBOutlet private var selectCurrencyLabel: UILabel!
    @IBOutlet private var selectCurrencyControl: UISegmentedControl!
    @IBOutlet private var selectLimitLabel: UILabel!
    @IBOutlet private var selectLimitTextField: UITextField!
    @IBOutlet private var authorLabel: UILabel!
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        rightBarButtonType = .hidden
        localizeUI()
        setupUI()
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        endEditing()
    }
    
    // MARK: - Actions

    override func leftBarButtonPressed() {
        saveLimit()
        dismiss()
    }
    
    private func saveLimit() {
        guard let text = selectLimitTextField.text, let limit = Int(text) else { return }
        userDefaults.set(limit, forKey: Constants.UI.currenciesLimitKey)
        print("Did update limit to: ", limit)
    }
    
    private func dismiss() {
        endEditing()
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func didSelectValue() {
        endEditing()
        saveCurrency()
    }
    
    private func saveCurrency() {
        let index = selectCurrencyControl.selectedSegmentIndex
        userDefaults.set(index, forKey: Constants.UI.selectedCurrencyIndexKey)
        print("Did update currency to: ", CurrencyType.types[index])
    }
    
    // MARK: - Instance Methods
    
    private func localizeUI() {
        setTitle("Settings")
        selectCurrencyLabel.text = "Select currency:"
        selectLimitLabel.text = "Limit number of currencies:"
        authorLabel.text = "Author: Toni Kocjan"
    }
    
    private func setupUI() {
        styleUI()
        setupSegmentControl()
        setupTextField()
    }
    
    private func styleUI() {
        selectCurrencyLabel.style(size: 15, type: .regular, color: .greyishBrown)
        selectLimitLabel.style(size: 15, type: .regular, color: .greyishBrown)
        authorLabel.style(size: 13, type: .regular, color: .warmGrey)
    }
    
    private func setupSegmentControl() {
        selectCurrencyControl.tintColor = .bitcoinOrange
        selectCurrencyControl.addTarget(self, action: #selector(didSelectValue), for: .valueChanged)
        selectCurrencyControl.removeAllSegments()
        CurrencyType.types.reversed().forEach { currency in
            selectCurrencyControl.insertSegment(withTitle: currency.rawValue, at: 0, animated: false)
        }
        selectValueInCurrencyControl()
    }
    
    private func selectValueInCurrencyControl() {
        let selectedValueIndex = (userDefaults.value(forKey: Constants.UI.selectedCurrencyIndexKey) as? Int) ?? 0
        selectCurrencyControl.selectedSegmentIndex = selectedValueIndex
        userDefaults.set(selectedValueIndex, forKey: Constants.UI.selectedCurrencyIndexKey)
    }
    
    private func setupTextField() {
        selectLimitTextField.placeholder = "Limit"
        selectLimitTextField.keyboardType = .decimalPad
        selectLimitTextField.font = .create(type: .regular, size: 13)
        setCurrentLimit()
    }
    
    private func setCurrentLimit() {
        let limit = getLimitFromUserDefaultsOrDefault()
        userDefaults.set(limit, forKey: Constants.UI.currenciesLimitKey)
        selectLimitTextField.text = String(limit)
    }
    
    private func getLimitFromUserDefaultsOrDefault() -> Int {
        return (userDefaults.value(forKey: Constants.UI.currenciesLimitKey) as? Int) ?? Constants.UI.defaultCurrenciesLimit
    }
    
    private func endEditing() {
        selectLimitTextField.endEditing(true)
    }
}

// MARK: - UITextFieldDelegate

extension SettingsViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return string != "."
    }
}
