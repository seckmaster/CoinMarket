//
//  CurrencyListDataBuilderTests.swift
//  CurrencyListDataBuilderTests
//
//  Created by Toni Kocjan on 05/11/2017.
//  Copyright © 2017 Toni Kocjan. All rights reserved.
//

import XCTest
@testable import CoinMarket

class CurrencyListDataBuilderTests: XCTestCase {

    var dataBuilder: CurrencyDataBuilderProtocol!
    var mockCurrency: Currency!

    override func setUp() {
        super.setUp()
        
        dataBuilder = CurrencyListDataBuilder()
        mockCurrency = loadJson().toCurrencies()!.first!
    }

    override func tearDown() {
        dataBuilder = nil
        super.tearDown()
    }
    
    private func loadJson() -> Data {
        let testBundle = Bundle(for: type(of: self))
        let path = testBundle.path(forResource: "coinMarket", ofType: "json")
        let data = try? Data(contentsOf: URL(fileURLWithPath: path!), options: .alwaysMapped)
        return data!
    }
    
    func testBuilderWithEmptyCurrency() {
        let data = dataBuilder.build(from: Currency(), convertToCurrency: nil)
        
        XCTAssertEqual(data.count, 4)
        data.forEach { XCTAssertNil($0.value) }
    }
    
    func testBuilderWithoutConvert() {
        let data = dataBuilder.build(from: mockCurrency, convertToCurrency: nil)
        test(data: data)
        XCTAssertEqual(data[2].key, "Price [USD]:")
        XCTAssertEqual(data[2].value, mockCurrency.priceUsd)
    }
    
    func testBuilderWithoutConvertUsd() {
        let data = dataBuilder.build(from: mockCurrency, convertToCurrency: CurrencyType.USD)
        test(data: data)
        XCTAssertEqual(data[2].key, "Price [USD]:")
        XCTAssertEqual(data[2].value, mockCurrency.priceUsd)
    }
    
    func testBuilderConvertEur() {
        let data = dataBuilder.build(from: mockCurrency, convertToCurrency: CurrencyType.EUR)
        test(data: data)
        XCTAssertEqual(data[2].key, "Price [EUR]:")
        XCTAssertEqual(data[2].value, mockCurrency.priceEur)
    }
    
    func testBuilderConvertCny() {
        let data = dataBuilder.build(from: mockCurrency, convertToCurrency: CurrencyType.CNY)
        test(data: data)
        XCTAssertEqual(data[2].key, "Price [CNY]:")
        XCTAssertEqual(data[2].value, mockCurrency.priceCny)
    }
    
    private func test(data: [KeyValue]) {
        XCTAssertEqual(data.count, 4)
        XCTAssertEqual(data[0].key, "Rank:")
        XCTAssertEqual(data[0].value, mockCurrency.rank)
        XCTAssertEqual(data[1].key, "Symbol:")
        XCTAssertEqual(data[1].value, mockCurrency.symbol)
        XCTAssertEqual(data[3].key, "24h change:")
        XCTAssertEqual(data[3].value, mockCurrency.percentChange_24h)
    }
}

