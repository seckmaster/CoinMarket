//
//  CustomNavigationViewController.swift
//  CoinMarket
//
//  Created by Toni Kocjan on 04/11/2017.
//  Copyright © 2017 Toni Kocjan. All rights reserved.
//

import UIKit

class CustomNavigationController: UINavigationController {

    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        interactivePopGestureRecognizer?.delegate = nil
        styleNavigationBar()
        renderBorder()
    }

    // MARK: - Instance Methods
    
    private func styleNavigationBar() {
        navigationBar.barTintColor = .white
        navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont.create(type: .semibold, size: 15),
                                             NSAttributedStringKey.foregroundColor: UIColor.white]
        navigationBar.setBackgroundImage(UIImage(), for: .top, barMetrics: .default)
        navigationBar.shadowImage = UIImage()
        navigationBar.tintColor = .white
        navigationBar.barTintColor = .bitcoinOrange
        navigationBar.backgroundColor = .bitcoinOrange
        navigationBar.layer.masksToBounds = false
    }
    
    private func renderBorder() {
        navigationBar.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        navigationBar.layer.shadowColor = UIColor.warmGrey.cgColor
        navigationBar.layer.shadowOpacity = 0.8
        navigationBar.layer.shadowRadius = 2
    }
    
}
