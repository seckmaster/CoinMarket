//
//  BaseViewController.swift
//  CoinMarket
//
//  Created by Toni Kocjan on 05/11/2017.
//  Copyright © 2017 Toni Kocjan. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    
    // MARK: - Properties
    
    var leftBarButtonType = NavigationBarButtonType.back {
        didSet { setLeftBarButton() }
    }
    var rightBarButtonType = NavigationBarButtonType.settings {
        didSet { setRightBarButton() }
    }
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
    }
    
    // MARK: - Actions
    
    @IBAction func leftBarButtonPressed() {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func rightBarButtonPressed() {
        navigationController?.performSegue(withIdentifier: .showSettingsSegue)
    }
    
    // MARK: - Instance Methods

    func setTitle(_ title: String) {
        self.title = title
        self.navigationItem.title = title.uppercased()
    }
    
    private func setupNavigationBar() {
        setLeftBarButton()
        setRightBarButton()
    }
}

// MARK: - Navigatable

extension BaseViewController: Navigatable {
    
    internal func setLeftBarButton() {
        guard let iconName = leftBarButtonType.iconName else {
            hideLeftBarButton()
            return
        }
        navigationItem.leftBarButtonItem = barButtonItem(for: UIImage(named: iconName), action: #selector(leftBarButtonPressed))
    }
    
    internal func setRightBarButton() {
        guard let iconName = rightBarButtonType.iconName else {
            hideRightBarButton()
            return
        }
        navigationItem.rightBarButtonItem = barButtonItem(for: UIImage(named: iconName), action: #selector(rightBarButtonPressed))
    }
    
    private func barButtonItem(for image: UIImage?, action: Selector) -> UIBarButtonItem {
        let button = UIButton(type: .custom)
        button.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
        button.setImage(image, for: .normal)
        button.addTarget(self, action: action, for: .touchUpInside)
        return UIBarButtonItem(customView: button)
    }
}
