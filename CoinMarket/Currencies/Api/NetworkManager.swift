//
//  NetworkManager.swift
//  CoinMarket
//
//  Created by Toni Kocjan on 04/11/2017.
//  Copyright © 2017 Toni Kocjan. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper
import ObjectMapper

class CoinMarketApi: CoinMarketApiProtocol {
    
    static let shared = CoinMarketApi()
    
    private var limit: Int?
    private var currencyId: String?
    private var convertToCurrency: String?
    private var urlBuilder: UrlBuilderProtocol?
    
    func consumeCoinMarketCapApi(
        limit: Int?=nil,
        currencyId: String?=nil,
        convertToCurrency: String?=nil,
        urlBuilder: UrlBuilderProtocol,
        completion: @escaping CompletionHandler) {
        
        self.limit = limit
        self.currencyId = currencyId
        self.convertToCurrency = convertToCurrency
        self.urlBuilder = urlBuilder
        
        guard let url = buildUrl() else {
            completion(nil, 0)
            return
        }
        
        request(url: url, completion: completion)
    }
    
    private func buildUrl() -> URLConvertible? {
        guard let urlBuilder = urlBuilder else { return nil }
        urlBuilder.baseUrl = Constants.Api.coinMarketUrl
        urlBuilder.limit = limit
        urlBuilder.currencyId = currencyId
        urlBuilder.convertToCurrency = convertToCurrency
        if let urlString = urlBuilder.build() {
            return try? urlString.asURL()
        }
        return nil
    }
    
    private func request(url: URLConvertible, completion: @escaping CompletionHandler) {
        Alamofire.request(url)
            .responseArray { (response: DataResponse<[Currency]>) in
                let statusCode = response.response?.statusCode ?? 0
                if let currencies = response.result.value, statusCode == 200 {
                    completion(currencies, statusCode)
                }
                else {
                    completion(nil, statusCode)
                }
        }
    }
}

