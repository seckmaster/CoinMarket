//
//  Refreshable.swift
//  CoinMarket
//
//  Created by Toni Kocjan on 04/11/2017.
//  Copyright © 2017 Toni Kocjan. All rights reserved.
//

import UIKit

protocol Refreshable {
    
    var tableView: UITableView! { get set }
    var refreshControl: UIRefreshControl! { get set }
    func setupRefreshControl()
    func startRefreshing()
    func stopRefreshing()
}

extension Refreshable where Self: UIViewController {
    
    func startRefreshing() {
        refreshControl.beginRefreshing()
        tableView.setContentOffset(CGPoint(x: tableView.contentOffset.x, y: -refreshControl.frame.height), animated: true)
    }
    
    func stopRefreshing() {
        DispatchQueue.main.async {
            self.refreshControl.endRefreshing()
        }
        tableView.reloadData()
    }
}
