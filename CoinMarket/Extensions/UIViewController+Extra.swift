//
//  UIViewController+Extra.swift
//  CoinMarket
//
//  Created by Toni Kocjan on 04/11/2017.
//  Copyright © 2017 Toni Kocjan. All rights reserved.
//

import UIKit

enum SegueIdentifier: String {
    case showCurrencyDetails = "ShowCurrencyDetailsSegue"
    case showSettingsSegue = "ShowSettingsSegue"
    
    func isSegue(_ segue: UIStoryboardSegue) -> Bool {
        if let identifier = segue.identifier, identifier == rawValue { return true }
        return false
    }
}

extension UIViewController {
    
    func canPerformSegue(withIdentifier identifier: String) -> Bool {
        let segues = self.value(forKey: "storyboardSegueTemplates") as? [NSObject]
        let filtered = segues?.filter({ $0.value(forKey: "identifier") as? String == identifier })
        
        return (filtered?.count ?? 0) > 0
    }
    
    func performSegue(withIdentifier identifier: SegueIdentifier, sender: Any?=nil) {
        performSegue(withIdentifier: identifier.rawValue, sender: sender ?? self)
    }
}
