//
//  ApiFacade.swift
//  CoinMarket
//
//  Created by Toni Kocjan on 04/11/2017.
//  Copyright © 2017 Toni Kocjan. All rights reserved.
//

import Foundation

class CoinMarketApiProvider: CoinMarketApiProviderProtocol {
    
    let coinMarketApi: CoinMarketApiProtocol
    let urlBuilder: CoinMarketUrlBuilderProtocol
    
    init(coinMarketApi: CoinMarketApiProtocol, urlBuilder: CoinMarketUrlBuilderProtocol) {
        self.coinMarketApi = coinMarketApi
        self.urlBuilder = urlBuilder
    }
    
    func getCurrencies(completion: @escaping CompletionHandler) {
        privateGetCurrencies(completion: completion)
    }
    
    func getCurrencies(limit: Int, completion: @escaping CompletionHandler) {
        privateGetCurrencies(limit: limit, completion: completion)
    }
    
    func getCurrencies(limit: Int, currencyId: String, completion: @escaping CompletionHandler) {
        privateGetCurrencies(limit: limit, currencyId: currencyId, completion: completion)
    }
    
    func getCurrencies(limit: Int, currencyId: String, convertToCurrency: String, completion: @escaping CompletionHandler) {
        privateGetCurrencies(limit: limit, currencyId: currencyId, convertToCurrency: convertToCurrency, completion: completion)
    }
    
    func getCurrencies(limit: Int, convertToCurrency: String, completion: @escaping CompletionHandler) {
        privateGetCurrencies(limit: limit, currencyId: nil, convertToCurrency: convertToCurrency, completion: completion)
    }
    
    private func privateGetCurrencies(limit: Int?=nil, currencyId: String?=nil, convertToCurrency: String?=nil, completion: @escaping CompletionHandler) {
        coinMarketApi.consumeCoinMarketCapApi(
            limit: limit,
            currencyId: currencyId,
            convertToCurrency: convertToCurrency,
            urlBuilder: urlBuilder,
            completion: completion)
    }
    
    func getCurrency(currencyId: String, completion: @escaping (Currency?, Int)->()) {
        privateGetCurrency(currencyId: currencyId, completion: completion)
    }
    
    func getCurrency(currencyId: String, convertToCurrency: String, completion: @escaping (Currency?, Int)->()) {
        privateGetCurrency(currencyId: currencyId, convertToCurrency: convertToCurrency, completion: completion)
    }

    private func privateGetCurrency(currencyId: String, convertToCurrency: String?=nil, completion: @escaping (Currency?, Int)->()) {
        privateGetCurrencies(
            limit: 1,
            currencyId: currencyId,
            convertToCurrency: convertToCurrency) { (currencies, code) in
                completion(currencies?.first, code)
        }
    }
}
