//
//  CurrencyFilterTests.swift
//  CurrencyFilterTests
//
//  Created by Toni Kocjan on 05/11/2017.
//  Copyright © 2017 Toni Kocjan. All rights reserved.
//

import XCTest
@testable import CoinMarket

class CurrencyFilterTests: XCTestCase {
    
    var currencyFilter: FilterProtocol!
    var currencies: [Currency]!
    
    override func setUp() {
        super.setUp()
        
        currencyFilter = CurrencyFilter(filter: CurrenciesViewController().filter)
        currencies = loadJson().toCurrencies()
    }
    
    private func loadJson() -> Data {
        let testBundle = Bundle(for: type(of: self))
        let path = testBundle.path(forResource: "coinMarket", ofType: "json")
        let data = try? Data(contentsOf: URL(fileURLWithPath: path!), options: .alwaysMapped)
        return data!
    }
    
    override func tearDown() {
        currencyFilter = nil
        super.tearDown()
    }

    func testEmptyQuery() {
        let filtered = currencyFilter.filter(searchQuery: "", currencies: currencies)
        XCTAssertEqual(filtered.count, currencies.count)
    }
    
    func testAddingLetters() {
        let c1 = currencyFilter.filter(searchQuery: "b", currencies: currencies).count
        let c2 = currencyFilter.filter(searchQuery: "bi", currencies: currencies).count
        let c3 = currencyFilter.filter(searchQuery: "bit", currencies: currencies).count
        let c4 = currencyFilter.filter(searchQuery: "bitc", currencies: currencies).count
        let c5 = currencyFilter.filter(searchQuery: "bitcoin", currencies: currencies).count
        XCTAssertEqual(19, c1)
        XCTAssertEqual(10, c2)
        XCTAssertEqual(8, c3)
        XCTAssertEqual(4, c4)
        XCTAssertEqual(3, c5)
    }
    
    func testRemovingLetters() {
        let c1 = currencyFilter.filter(searchQuery: "bitcoin", currencies: currencies).count
        let c2 = currencyFilter.filter(searchQuery: "bitc", currencies: currencies).count
        let c3 = currencyFilter.filter(searchQuery: "bit", currencies: currencies).count
        let c4 = currencyFilter.filter(searchQuery: "bi", currencies: currencies).count
        let c5 = currencyFilter.filter(searchQuery: "b", currencies: currencies).count
        XCTAssertEqual(3, c1)
        XCTAssertEqual(4, c2)
        XCTAssertEqual(8, c3)
        XCTAssertEqual(10, c4)
        XCTAssertEqual(19, c5)
    }
    
    func testWithRemovingQuery() {
        let c1 = currencyFilter.filter(searchQuery: "Bitcoin", currencies: currencies).count
        let c2 = currencyFilter.filter(searchQuery: "", currencies: currencies).count
        XCTAssertEqual(3, c1)
        XCTAssertEqual(c2, currencies.count)
    }
    
    func testInsertion() {
        let c1 = currencyFilter.filter(searchQuery: "tco", currencies: currencies).count
        let c2 = currencyFilter.filter(searchQuery: "tcoi", currencies: currencies).count
        let c3 = currencyFilter.filter(searchQuery: "tcoin", currencies: currencies).count
        XCTAssertEqual(6, c1)
        XCTAssertEqual(5, c2)
        XCTAssertEqual(5, c3)
    }
}
