//
//  Constants.swift
//  CoinMarket
//
//  Created by Toni Kocjan on 04/11/2017.
//  Copyright © 2017 Toni Kocjan. All rights reserved.
//

import Foundation

enum Constants {
    
    enum Api {
        private static let coinMarketBaseUrl = "https://api.coinmarketcap.com/v1/ticker/"
        static let coinMarketUrl = coinMarketBaseUrl
    }
    
    enum UI {
        static let selectedCurrencyIndexKey = "selectedCurrencyKey"
        static let currenciesLimitKey = "limitKey"
        static let defaultCurrenciesLimit = 100
    }
}
