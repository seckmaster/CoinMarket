//
//  CustomUserDefaults.swift
//  CoinMarketSettingsUITests
//
//  Created by Toni Kocjan on 05/11/2017.
//  Copyright © 2017 Toni Kocjan. All rights reserved.
//

import Foundation

class CustomUserDefaults: UserDefaults {
    
    var currencySetCount = 0
    var limitDidChange = false
    
    override func setValue(_ value: Any?, forKey key: String) {
        super.setValue(value, forKey: key)
        if key == "selectedCurrencyKey" {
            currencySetCount += 1
        }
        else if key == "limitKey" {
            limitDidChange = true
        }
    }

}
