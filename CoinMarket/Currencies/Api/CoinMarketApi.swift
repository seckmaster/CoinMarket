//
//  CoinMarketManager.swift
//  CoinMarket
//
//  Created by Toni Kocjan on 04/11/2017.
//  Copyright © 2017 Toni Kocjan. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper
import ObjectMapper

class CoinMarketApi: CoinMarketApiProtocol {
    
    static let shared = CoinMarketApi()
    
    private var limit: Int?
    private var currencyId: String?
    private var convertToCurrency: String?
    private var urlBuilder: CoinMarketUrlBuilderProtocol!
    
    func consumeCoinMarketCapApi(
        limit: Int?=nil,
        currencyId: String?=nil,
        convertToCurrency: String?=nil,
        urlBuilder: CoinMarketUrlBuilderProtocol,
        completion: @escaping CompletionHandler) {
        
        self.limit = limit
        self.currencyId = currencyId
        self.convertToCurrency = convertToCurrency
        self.urlBuilder = urlBuilder
        
        guard let url = buildUrl() else {
            completion(nil, 0)
            return
        }
        
        request(url: url, completion: completion)
    }
    
    private func buildUrl() -> URLConvertible? {
        updateBuilder()
        if let urlString = urlBuilder.build() {
            return try? urlString.asURL()
        }
        return nil
    }
    
    private func updateBuilder() {
        urlBuilder.baseUrl = Constants.Api.coinMarketUrl
        urlBuilder.limit = limit
        urlBuilder.currencyId = currencyId
        urlBuilder.convertToCurrency = convertToCurrency
    }
    
    private func request(url: URLConvertible, completion: @escaping CompletionHandler) {
        print("Requesting: ", url)
        Alamofire.request(url)
            .responseArray { (response: DataResponse<[Currency]>) in
                let statusCode = response.response?.statusCode ?? 0
                print("  --> Status code: ", statusCode)
                if let currencies = response.result.value, statusCode == 200 {
                    print("  --> Count: ", currencies.count)
                    completion(currencies, statusCode)
                }
                else {
                    print("  --> Response is nil")
                    completion(nil, statusCode)
                }
        }
    }
}

