//
//  ApiFacadeTests.swift
//  ApiFacadeTests
//
//  Created by Toni Kocjan on 04/11/2017.
//  Copyright © 2017 Toni Kocjan. All rights reserved.
//

import XCTest
@testable import CoinMarket

class ApiFacadeTests: XCTestCase {
    
    var apiFacade: CoinMarketApiProvider!
    
    override func setUp() {
        super.setUp()
        
        apiFacade = CoinMarketApiProvider(coinMarketApi: CoinMarketApi(), urlBuilder: UrlBuilder())
    }
    
    override func tearDown() {
        apiFacade = nil
        super.tearDown()
    }
    
    func testGetCurrencies() {
        let promise = expectation(description: "Completion handler invoked")
        var responseCode: Int?
        var responseCurrencies: [Currency]?
        
        apiFacade.getCurrencies { (currencies, code) in
            responseCode = code
            responseCurrencies = currencies
            promise.fulfill()
        }
        waitForExpectations(timeout: 5, handler: nil)
        
        XCTAssertEqual(responseCode, 200)
        XCTAssertNotNil(responseCurrencies)
    }
    
    func testGetCurrenciesWithLimitAndCurrencyIdAndConvertCurrency() {
        let promise = expectation(description: "Completion handler invoked")
        var responseCode: Int?
        var responseCurrencies: [Currency]?
        
        apiFacade.getCurrencies(limit: 10, currencyId: "ethereum", convertToCurrency: "EUR") { (currencies, code) in
            responseCode = code
            responseCurrencies = currencies
            promise.fulfill()
        }
        waitForExpectations(timeout: 5, handler: nil)
        
        XCTAssertEqual(responseCode, 200)
        XCTAssertNotNil(responseCurrencies)
    }
    
    func testGetCurrency() {
        let promise = expectation(description: "Completion handler invoked")
        var responseCode: Int?
        var responseCurrency: Currency?
        
        apiFacade.getCurrency(currencyId: "ethereum") { (currency, code) in
            responseCode = code
            responseCurrency = currency
            promise.fulfill()
        }
        waitForExpectations(timeout: 5, handler: nil)
        
        XCTAssertEqual(responseCode, 200)
        XCTAssertNotNil(responseCurrency)
    }
    
    func testGetCurrencyWithLimitAndCurrencyIdAndConvertCurrency() {
        let promise = expectation(description: "Completion handler invoked")
        var responseCode: Int?
        var responseCurrency: Currency?
        
        apiFacade.getCurrency(currencyId: "bitcoin", convertToCurrency: "CNY") { (currency, code) in
            responseCode = code
            responseCurrency = currency
            promise.fulfill()
        }
        waitForExpectations(timeout: 5, handler: nil)
        
        XCTAssertEqual(responseCode, 200)
        XCTAssertNotNil(responseCurrency)
    }
}
