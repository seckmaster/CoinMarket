//
//  UrlBuilder.swift
//  CoinMarket
//
//  Created by Toni Kocjan on 04/11/2017.
//  Copyright © 2017 Toni Kocjan. All rights reserved.
//

import Foundation

class UrlBuilder: CoinMarketUrlBuilderProtocol {
    
    var baseUrl: String?
    var currencyId: String?
    var convertToCurrency: String?
    var limit: Int?
    
    func build() -> String? {
        guard let baseUrl = baseUrl else { return nil }
        
        var url = baseUrl
        if let currencyId = currencyId {
            url += currencyId + "/"
        }
        if let convertToCurrency = convertToCurrency {
            url += "?convert=" + convertToCurrency
        }
        if let limit = limit {
            let delimiter = convertToCurrency == nil ? "?limit=" : "&limit="
            url += delimiter + String(limit)
        }
        return url
    }
    
}
