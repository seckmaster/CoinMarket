//
//  UITableView+Extra.swift
//  CoinMarket
//
//  Created by Toni Kocjan on 04/11/2017.
//  Copyright © 2017 Toni Kocjan. All rights reserved.
//

import UIKit

enum UITableViewCellIdentifier: String {
    case currencyCell = "CurrencyCell"
    case currencyDetailsCell = "CurrencyDetailsCell"
    case currencyKeyValueCell = "CurrencyKeyValueCell"
}

extension UITableView {
    
    func dequeueReusableCell(withIdentifier identifier: UITableViewCellIdentifier) -> UITableViewCell? {
        return dequeueReusableCell(withIdentifier: identifier.rawValue)
    }
}
