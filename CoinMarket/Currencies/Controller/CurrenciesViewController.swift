//
//  ViewController.swift
//  CoinMarket
//
//  Created by Toni Kocjan on 04/11/2017.
//  Copyright © 2017 Toni Kocjan. All rights reserved.
//

import UIKit

class CurrenciesViewController: RefreshableTableViewController {
    
    // MARK: - Properties
    
    private var searchBar: UISearchBar!
    private var currencies: [Currency]?
    private var filteredCurrencies: [Currency]? // result of querying using searchBar
    
    var apiProvider: CoinMarketApiProviderProtocol!
    var currencyFilter: FilterProtocol!
    var dataBuilder: CurrencyDataBuilderProtocol!

    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initializeDependencies()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        hideLeftBarButton()
        reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if SegueIdentifier.showCurrencyDetails.isSegue(segue), let currencyDetailsViewController = segue.destination as? CurrencyDetailsViewController {
            passDataTo(currencyDetailsViewController: currencyDetailsViewController)
        }
    }
    
    func passDataTo(currencyDetailsViewController: CurrencyDetailsViewController) {
        guard let row = tableView.indexPathForSelectedRow?.row else { return }
        currencyDetailsViewController.apiProvider = apiProvider
        currencyDetailsViewController.currency = filteredCurrencies?[row]
        currencyDetailsViewController.dataBuilder = CurrencyDetailsDataBuilder()
    }
    
    // MARK: - Override Methods
    
    override func shouldReloadData() -> Bool {
        // (re)load data if not yet loaded or any of the filters changed
        if convertToCurrencyFromUserDefaultsOrDefault() != currentCurrency { return true }
        if currenciesLimitFromUserDefaultsOrDefault() != currentLimit { return true }
        if currencies == nil { return true }
        return false
    }
    
    override func loadData() {
        apiProvider.getCurrencies(limit: currentLimit, convertToCurrency: currentCurrency.rawValue) { (currencies, code) in
            guard code == 200, let currencies = currencies, !currencies.isEmpty else {
                self.stopRefreshing()
                return
            }
            self.currencies = currencies
            self.filterCurrencies(searchQuery: self.searchBar.text ?? "")
            self.stopRefreshing()
        }
    }
    
    // MARK: - Instance Methods
    
    private func initializeDependencies() {
        apiProvider = CoinMarketApiProvider(coinMarketApi: CoinMarketApi(), urlBuilder: UrlBuilder())
        currencyFilter = CurrencyFilter(filter: filter)
        dataBuilder = CurrencyListDataBuilder()
    }
    
    private func setupUI() {
        setTitle("Currencies")
        setupTableView()
    }
    
    private func setupTableView() {
        tableView.keyboardDismissMode = .onDrag
        createSearchBarHeader()
    }
    
    private func createSearchBarHeader() {
        searchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 56))
        searchBar.delegate = self
        tableView.tableHeaderView = searchBar
    }
    
    private func showCurrencyDetails() {
        performSegue(withIdentifier: .showCurrencyDetails)
    }
    
    private func filterCurrencies(searchQuery: String) {
        guard let currencies = currencies else { return }
        filteredCurrencies = currencyFilter.filter(searchQuery: searchQuery, currencies: currencies)
        tableView.reloadData()
    }
    
    // MARK: - FilterCallback
    
    func filter(_ searchQuery: String) -> (_ currency: Currency) -> Bool {
        return { currency in
            let lowercasedQuery = searchQuery.lowercased()
            if let name = currency.name, name.lowercased().contains(lowercasedQuery) {
                return true
            }
            if let symbol = currency.symbol, symbol.lowercased().contains(lowercasedQuery) {
                return true
            }
            return false
        }
    }
}

// MARK: - UITableViewDataSource

extension CurrenciesViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return filteredCurrencies == nil ? 0 : 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredCurrencies?.count ?? 0
    }
}

// MARK: - UITableViewDelegate

extension CurrenciesViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let currencyKeyValueCell = tableView.dequeueReusableCell(withIdentifier: .currencyKeyValueCell) as! CurrencyKeyValueCell
        currencyKeyValueCell.data = dataModel(for: currency(at: indexPath))
        return currencyKeyValueCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let currencies = filteredCurrencies, indexPath.row < currencies.count else { return }
        showCurrencyDetails()
    }
    
    internal func currency(at indexPath: IndexPath) -> Currency? {
        return filteredCurrencies?[indexPath.row]
    }
    
    internal func dataModel(for currency: Currency?) -> [KeyValue]? {
        if let currency = currency {
            return dataBuilder.build(from: currency, convertToCurrency: currentCurrency)
        }
        return nil
    }
}

// MARK: - UISearchBarDelegate

extension CurrenciesViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print("Search query: ", searchText)
        filterCurrencies(searchQuery: searchText)
    }
}
