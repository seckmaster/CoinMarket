//
//  CurrencyFilter.swift
//  CoinMarket
//
//  Created by Toni Kocjan on 05/11/2017.
//  Copyright © 2017 Toni Kocjan. All rights reserved.
//

import Foundation

typealias FilterCallback = (_ searchQuery: String) -> (_ currency: Currency) -> Bool

class CurrencyFilter: FilterProtocol {
    
    private(set) var filter: FilterCallback
    
    init(filter: @escaping FilterCallback) {
        self.filter = filter
    }
    
    func filter(searchQuery query: String, currencies: [Currency]) -> [Currency] {
        if query.isEmpty { return currencies }
        return currencies.filter(filter(query))
    }
}
