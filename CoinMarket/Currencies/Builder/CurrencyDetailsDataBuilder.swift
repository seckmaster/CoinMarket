//
//  CurrencyDetailsDataBuilder.swift
//  CoinMarket
//
//  Created by Toni Kocjan on 05/11/2017.
//  Copyright © 2017 Toni Kocjan. All rights reserved.
//

import Foundation

class CurrencyDetailsDataBuilder: CurrencyDataBuilderProtocol {
    
    func build(from currency: Currency, convertToCurrency convert: CurrencyType?) -> [KeyValue] {
        let priceCurrency = convert?.rawValue ?? CurrencyType.USD.rawValue
        
        var data = [KeyValue]()
        data.append((key: "Rank:", value: currency.rank))
        data.append((key: "Name:", value: currency.name))
        data.append((key: "Symbol:", value: currency.symbol))
        data.append((key: "Price [\(priceCurrency)]:", value: price(for: currency, convertToCurrency: convert)))
        data.append((key: "24h volume [\(priceCurrency)]:", value: volume(for: currency, convertToCurrency: convert)))
        data.append((key: "Market cap [\(priceCurrency)]:", value: marketCap(for: currency, convertToCurrency: convert)))
        data.append((key: "Price [BTC]:", value: currency.priceBtc))
        data.append((key: "1h change:", value: currency.percentChange_1h))
        data.append((key: "24h change:", value: currency.percentChange_24h))
        data.append((key: "7d change:", value: currency.percentChange_7d))
        data.append((key: "Total supply:", value: currency.totalSupply))
        data.append((key: "Available supply:", value: currency.availableSupply))
        return data
    }
    
    private func price(for currency: Currency, convertToCurrency convert: CurrencyType?) -> String? {
        if let convert = convert {
            if convert == .CNY { return currency.priceCny }
            if convert == .EUR { return currency.priceEur }
        }
        return currency.priceUsd
    }
    
    private func volume(for currency: Currency, convertToCurrency convert: CurrencyType?) -> String? {
        if let convert = convert {
            if convert == .CNY { return currency.volume24hCny }
            if convert == .EUR { return currency.volume24hEur }
        }
        return currency.volume24hUsd
    }
    
    private func marketCap(for currency: Currency, convertToCurrency convert: CurrencyType?) -> String? {
        if let convert = convert {
            if convert == .CNY { return currency.marketCapCny }
            if convert == .EUR { return currency.marketCapEur }
        }
        return currency.marketCapUsd
    }
}
