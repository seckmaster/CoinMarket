//
//  Font+Extra.swift
//  CoinMarket
//
//  Created by Toni Kocjan on 04/11/2017.
//  Copyright © 2017 Toni Kocjan. All rights reserved.
//

import UIKit

public enum FontType: String {
    case regular = "Regular", bold = "Bold", semibold = "Semibold", italic = "Italic", light = "Light"
}

extension UIFont {
    
    static let fontFamilyName = "OpenSans"
    
    static func create(type: FontType, size: CGFloat) -> UIFont {
        var fontName = fontFamilyName
        if type != .regular {
            fontName += "-"+type.rawValue
        }
        
        if let font = UIFont(name: fontName, size: size) {
            return font
        }
        
        return .systemFont(ofSize: size)
    }
}
