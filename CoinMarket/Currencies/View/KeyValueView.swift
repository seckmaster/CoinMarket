//
//  KeyValueView.swift
//  CoinMarket
//
//  Created by Toni Kocjan on 05/11/2017.
//  Copyright © 2017 Toni Kocjan. All rights reserved.
//

import UIKit
import SnapKit

class KeyValueView: UIView {
    
    private lazy var stackView = UIStackView()
    private(set) lazy var keyLabel = UILabel()
    private(set) lazy var valueLabel = UILabel()
    private let inset: CGFloat = 0
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
    
    func initialize() {
        stackView.axis = .horizontal
        stackView.distribution = .equalSpacing
        stackView.alignment = .center
        stackView.spacing = 20
        stackView.addArrangedSubview(keyLabel)
        stackView.addArrangedSubview(valueLabel)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(stackView)
        addConstraints()
    }
    
    private func addConstraints() {
        stackView.snp.makeConstraints { (make) in
            make.top.equalToSuperview().inset(inset)
            make.bottom.equalToSuperview().inset(inset)
            make.left.equalToSuperview().inset(inset)
            make.right.equalToSuperview().inset(inset)
        }
    }
}
