//
//  CoinMarketSettingsUITests.swift
//  CoinMarketSettingsUITests
//
//  Created by Toni Kocjan on 05/11/2017.
//  Copyright © 2017 Toni Kocjan. All rights reserved.
//

import XCTest
@testable import CoinMarket

//class CoinMarketSettingsUITests: XCTestCase {
//
//    var app: XCUIApplication!
//    var userDefaults: CustomUserDefaults!
//    var settings: SettingsViewController!
//
//    override func setUp() {
//        super.setUp()
//
//        userDefaults = CustomUserDefaults()
//        settings = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController() as! SettingsViewController!
//        settings.userDefaults = userDefaults
//        app = XCUIApplication()
//        app.launch()
//
//        // In UI tests it is usually best to stop immediately when a failure occurs.
//        continueAfterFailure = false
//        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
//        XCUIApplication().launch()
//
//        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
//    }
//
//    override func tearDown() {
//        app = nil
//        super.tearDown()
//    }
//
//    func testExample() {
//        app.navigationBars["Currencies"].buttons["iconSetting"].tap()
//
//        XCTAssertEqual(100, userDefaults.value(forKey: "limitKey") as? Int)
//        XCTAssertEqual("USD", userDefaults.value(forKey: "selectedCurrencyKey") as? String)
//
//        app/*@START_MENU_TOKEN@*/.buttons["EUR"]/*[[".segmentedControls.buttons[\"EUR\"]",".buttons[\"EUR\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
//        app/*@START_MENU_TOKEN@*/.buttons["CNY"]/*[[".segmentedControls.buttons[\"CNY\"]",".buttons[\"CNY\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
//        app/*@START_MENU_TOKEN@*/.buttons["USD"]/*[[".segmentedControls.buttons[\"USD\"]",".buttons[\"USD\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
//        app.buttons["CNY"].tap()
//
//        XCTAssertEqual("CNY", userDefaults.value(forKey: "selectedCurrencyKey") as? String)
//
//        let limitTextField = app.textFields["Limit"]
//        limitTextField.tap()
//        limitTextField.typeText("250")
//        app.children(matching: .window).element(boundBy: 0).children(matching: .other).element.tap()
//
//        XCTAssertEqual(100, userDefaults.value(forKey: "limitKey") as? Int)
//
//        app.navigationBars["Settings"].buttons["backButton"].tap()
//
//        XCTAssertEqual(250, userDefaults.value(forKey: "limitKey") as? Int)
//        XCTAssertTrue(userDefaults.limitDidChange)
//    }
//
//}
