//
//  Currency.swift
//  CoinMarket
//
//  Created by Toni Kocjan on 04/11/2017.
//  Copyright © 2017 Toni Kocjan. All rights reserved.
//

import Foundation
import ObjectMapper

enum CurrencyType: String {
    case EUR, USD, CNY
    
    static var types: [CurrencyType] {
        get {
            return [.USD, .EUR, .CNY]
        }
    }
}

class Currency: Mappable {
    
    private(set) var id: String?
    private(set) var name: String?
    private(set) var symbol: String?
    private(set) var rank: String?
    private(set) var priceEur: String?
    private(set) var priceUsd: String?
    private(set) var priceCny: String?
    private(set) var priceBtc: String?
    private(set) var volume24hEur: String?
    private(set) var volume24hUsd: String?
    private(set) var volume24hCny: String?
    private(set) var marketCapEur: String?
    private(set) var marketCapUsd: String?
    private(set) var marketCapCny: String?
    private(set) var percentChange_1h: String?
    private(set) var percentChange_24h: String?
    private(set) var percentChange_7d: String?
    private(set) var totalSupply: String?
    private(set) var availableSupply: String?
    
    required convenience init(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        symbol <- map["symbol"]
        rank <- map["rank"]
        priceBtc <- map["price_btc"]
        priceEur <- map["price_eur"]
        priceUsd <- map["price_usd"]
        priceCny <- map["price_cny"]
        volume24hEur <- map["24h_volume_eur"]
        volume24hUsd <- map["24h_volume_usd"]
        volume24hCny <- map["24h_volume_cny"]
        marketCapEur <- map["market_cap_eur"]
        marketCapUsd <- map["market_cap_usd"]
        marketCapCny <- map["market_cap_cny"]
        percentChange_1h <- map["percent_change_1h"]
        percentChange_24h <- map["percent_change_24h"]
        percentChange_7d <- map["percent_change_7d"]
        totalSupply <- map["total_supply"]
        availableSupply <- map["available_supply"]
    }
}
