//
//  CurrencyDetailsViewController.swift
//  CoinMarket
//
//  Created by Toni Kocjan on 04/11/2017.
//  Copyright © 2017 Toni Kocjan. All rights reserved.
//

import UIKit

class CurrencyDetailsViewController: RefreshableTableViewController {

    // MARK: - Properties
    
    var currency: Currency?
    var apiProvider: CoinMarketApiProviderProtocol!
    var dataBuilder: CurrencyDataBuilderProtocol!
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setTitle(currency?.name ?? "Unknown currency")
        styleTableView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        reloadData()
    }
    
    // MARK: - Instance Methods
    
    override func shouldReloadData() -> Bool {
        // (re)load data if convert currency changed
        return convertToCurrencyFromUserDefaultsOrDefault() != currentCurrency
    }
    
    override func loadData() {
        guard let currency = currency, let id = currency.id else { return }

        apiProvider.getCurrency(currencyId: id, convertToCurrency: currentCurrency.rawValue) { (currency, code) in
            guard code == 200, let currency = currency else {
                self.stopRefreshing()
                return
            }
            self.currency = currency
            self.stopRefreshing()
        }
    }
    
    private func styleTableView() {
        tableView.separatorStyle = .none
        tableView.allowsSelection = false
    }
}

// MARK: - UITableViewDataSource

extension CurrencyDetailsViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return currency == nil ? 0 : 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
}

// MARK: - UITableViewDelegate

extension CurrencyDetailsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let currencyDetailsCell = tableView.dequeueReusableCell(withIdentifier: .currencyKeyValueCell) as! CurrencyKeyValueCell
        if let currency = currency {
            currencyDetailsCell.data = dataBuilder.build(from: currency, convertToCurrency: currentCurrency)
        }
        return currencyDetailsCell
    }
}
