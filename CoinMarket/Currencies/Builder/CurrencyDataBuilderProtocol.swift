//
//  CurrencyDataBuilderProtocol.swift
//  CoinMarket
//
//  Created by Toni Kocjan on 05/11/2017.
//  Copyright © 2017 Toni Kocjan. All rights reserved.
//

import Foundation

protocol CurrencyDataBuilderProtocol {
    func build(from currency: Currency, convertToCurrency convert: CurrencyType?) -> [KeyValue]
}
