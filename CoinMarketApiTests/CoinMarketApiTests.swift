//
//  CoinMarketApiTests.swift
//  CoinMarketApiTests
//
//  Created by Toni Kocjan on 04/11/2017.
//  Copyright © 2017 Toni Kocjan. All rights reserved.
//

import XCTest
@testable import CoinMarket

class CoinMarketApiTests: XCTestCase {
    
    var coinMarketApi: CoinMarketApiProtocol!
    
    override func setUp() {
        super.setUp()
        initApi()
    }
    
    private func initApi() {
        coinMarketApi = CoinMarketApi.shared
    }
    
    override func tearDown() {
        coinMarketApi = nil
        super.tearDown()
    }
    
    private func consumeApi(limit: Int?=nil, currencyId: String?=nil, convertToCurrency: String?=nil) -> (currencies: [Currency]?, code: Int?) {
        let promise = expectation(description: "Completion handler invoked")
        var responseCode: Int?
        var responseCurrencies: [Currency]?
        
        coinMarketApi.consumeCoinMarketCapApi(limit: limit, currencyId: currencyId, convertToCurrency: convertToCurrency, urlBuilder: UrlBuilder()) { (currencies, code) in
            responseCode = code
            responseCurrencies = currencies
            promise.fulfill()
        }
        waitForExpectations(timeout: 5, handler: nil)
        
        return (responseCurrencies, responseCode)
    }
    
    func testCoinMarketApiCallSuccess() {
        let response = consumeApi()
        let responseCurrencies = response.currencies
        let responseCode = response.code
        
        XCTAssertEqual(responseCode, 200)
        XCTAssertNotNil(responseCurrencies)
        testIfCurrencyIsSuccessfulyMapped(currency: responseCurrencies![0])
    }
    
    func testCoinMarketWithLimitApiCallSuccess() {
        let response = consumeApi(limit: 100)
        let responseCurrencies = response.currencies
        let responseCode = response.code
        
        XCTAssertEqual(responseCode, 200)
        XCTAssertNotNil(responseCurrencies)
        XCTAssertEqual(responseCurrencies?.count, 100)
    }
    
    func testCoinMarketCurrencyApiCallSuccess() {
        let response = consumeApi(currencyId: "bitcoin")
        let responseCurrencies = response.currencies
        let responseCode = response.code
        
        XCTAssertEqual(responseCode, 200)
        XCTAssertNotNil(responseCurrencies?.first)
        testIfCurrencyIsSuccessfulyMapped(currency: responseCurrencies?.first)
    }
    
    func testCoinMarketCurrencyConvertToEurApiCallSuccess() {
        let response = consumeApi(currencyId: "bitcoin", convertToCurrency: "EUR")
        let responseCurrencies = response.currencies
        let responseCode = response.code
        
        XCTAssertEqual(responseCode, 200)
        XCTAssertNotNil(responseCurrencies?.first)
        testIfCurrencyIsSuccessfulyMapped(currency: responseCurrencies?.first)
        testIfEurCurrencyIsSuccessfulyMapped(eurCurrency: responseCurrencies?.first)
    }
    
    func testCoinMarketCurrencyConvertToCnyWithLimitApiCallSuccess() {
        let response = consumeApi(limit: 1, currencyId: "ethereum", convertToCurrency: "CNY")
        let responseCurrencies = response.currencies
        let responseCode = response.code
        
        XCTAssertEqual(responseCode, 200)
        XCTAssertEqual(responseCurrencies?.count, 1)
        XCTAssertNotNil(responseCurrencies?.first)
        testIfCurrencyIsSuccessfulyMapped(currency: responseCurrencies?.first)
        testIfCnyCurrencyIsSuccessfulyMapped(cnyCurrency: responseCurrencies?.first)
    }
    
    private func testIfCurrencyIsSuccessfulyMapped(currency: Currency?) {
        guard let currency = currency else { return }
        XCTAssertNotNil(currency.id)
        XCTAssertNotNil(currency.name)
        XCTAssertNotNil(currency.symbol)
        XCTAssertNotNil(currency.rank)
        XCTAssertNotNil(currency.priceUsd)
        XCTAssertNotNil(currency.priceBtc)
        XCTAssertNotNil(currency.volume24hUsd)
        XCTAssertNotNil(currency.marketCapUsd)
        XCTAssertNotNil(currency.percentChange_1h)
        XCTAssertNotNil(currency.percentChange_7d)
        XCTAssertNotNil(currency.percentChange_24h)
        XCTAssertNotNil(currency.totalSupply)
        XCTAssertNotNil(currency.availableSupply)
    }
    
    private func testIfEurCurrencyIsSuccessfulyMapped(eurCurrency currency: Currency?) {
        guard let currency = currency else { return }
        XCTAssertNotNil(currency.priceEur)
        XCTAssertNotNil(currency.marketCapEur)
        XCTAssertNotNil(currency.volume24hEur)
    }
    
    private func testIfCnyCurrencyIsSuccessfulyMapped(cnyCurrency currency: Currency?) {
        guard let currency = currency else { return }
        XCTAssertNotNil(currency.priceCny)
        XCTAssertNotNil(currency.marketCapCny)
        XCTAssertNotNil(currency.volume24hCny)
    }
}
