//
//  UrlBuilderTests.swift
//  UrlBuilderTests
//
//  Created by Toni Kocjan on 04/11/2017.
//  Copyright © 2017 Toni Kocjan. All rights reserved.
//

import XCTest
@testable import CoinMarket

class UrlBuilderTests: XCTestCase {
    
    var builder: UrlBuilder!
    var baseUrl = Constants.Api.coinMarketUrl
    var currencyId = "bitcoin"
    var convertCurrency = "EUR"
    
    override func setUp() {
        super.setUp()
        
        builder = UrlBuilder()
    }
    
    override func tearDown() {
        builder = nil
        super.tearDown()
    }
    
    func testBuilderReturnsNil() {
        XCTAssertNil(builder.build())
    }
    
    func testOnlyBaseUrl() {
        builder.baseUrl = baseUrl
        XCTAssertEqual(baseUrl, builder.build())
    }
    
    func testWithCurrencyId() {
        builder.baseUrl = baseUrl
        builder.currencyId = currencyId
        XCTAssertEqual(baseUrl + currencyId + "/", builder.build())
    }
    
    func testWithCurrencyIdAndConvertCurrency() {
        builder.baseUrl = baseUrl
        builder.currencyId = currencyId
        builder.convertToCurrency = convertCurrency
        XCTAssertEqual(baseUrl + currencyId + "/" + "?convert=" + convertCurrency, builder.build())
    }
    
    func testWithConvertCurrency() {
        builder.baseUrl = baseUrl
        builder.convertToCurrency = convertCurrency
        XCTAssertEqual(baseUrl + "?convert=" + convertCurrency, builder.build())
    }
    
    func testCurrencyIdAndWithConvertCurrency() {
        builder.baseUrl = baseUrl
        builder.currencyId = currencyId
        builder.convertToCurrency = convertCurrency
        XCTAssertEqual(baseUrl + currencyId + "/" + "?convert=" + convertCurrency, builder.build())
    }
    
    func testWithCurrencyIdAndConvertCurrencyAndLimit() {
        builder.baseUrl = baseUrl
        builder.currencyId = currencyId
        builder.convertToCurrency = convertCurrency
        builder.limit = 10
        XCTAssertEqual(baseUrl + currencyId + "/" + "?convert=" + convertCurrency + "&limit=10", builder.build())
    }
    
    func testWithLimit() {
        builder.baseUrl = baseUrl
        builder.limit = 10
        XCTAssertEqual(baseUrl + "?limit=10", builder.build())
    }
    
    func testCurrencyIdAndLimit() {
        builder.baseUrl = baseUrl
        builder.currencyId = currencyId
        builder.limit = 10
        XCTAssertEqual(baseUrl + currencyId + "/" + "?limit=10", builder.build())
    }
}
